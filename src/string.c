
/* ==== String: Basic ==== */

/* ---- Basic ---- */

String c_string_to_string(char* s) {
    return (String) { (u8*) s, (u64) strlen(s) }; 
}

// note: not safe, use with caution
String string_advance(String s, u64 pos) {
    return (String) {s.data + pos, s.count - pos};
}

// note: not safe, use with caution (also end is exclusive)
String string_view(String s, u64 start, u64 end) {
    return (String) {s.data + start, end - start};
}

// todo: speed
u8 string_equal(String a, String b) {
    if (a.count != b.count) return 0;
    if (a.data  == b.data ) return 1;
    for (u64 i = 0; i < a.count; i++) {
        if (a.data[i] != b.data[i]) return 0; 
    }
    return 1;
}



/* ---- Basic Utils ---- */

const u8 string_spaces_lookup_table[256] = {
    [' ']  = 1,
    ['\t'] = 1,
    ['\r'] = 1,
    ['\n'] = 1,
};

void string_build_lookup_table(u8 table[256], String s) {
    for (u64 i = 0; i < s.count; i++) {
        table[s.data[i]] = 1;
    }
}




/* ---- String Match ---- */

u8 string_contains_u8(String s, u8 c) {
    for (u64 i = 0; i < s.count; i++) {
        if (s.data[i] == c) return 1; 
    }
    return 0;
}

// todo: validate
u8 string_contains_any_u8(String a, String match) {
    
    if (!a.count) return 0;
    
    u8 table[256];
    string_build_lookup_table(table, match);
    
    for (u64 i = 0; i < a.count; i++) {
        if (table[a.data[i]]) return 1;
    }
    
    return 0;
}

// naive search for now
// todo: validate
u8 string_contains(String a, String b) {
    if (a.count < b.count) return 0;
    for (u64 i = 0; i < a.count - b.count + 1; i++) {
        for (u64 j = 0; j < b.count; j++) {
            if (a.data[i + j] != b.data[j]) goto next;
        }
        return 1;
        next: continue;
    }
    return 0;
}

u8 string_starts_with_u8(String s, u8 c) {
    if (!s.count) return 0;
    return s.data[0] == c;
}

u8 string_ends_with_u8(String s, u8 c) {
    if (!s.count) return 0;
    return s.data[s.count - 1] == c;
}

u8 string_starts_with_any_u8(String s, String match) {
    
    if (!s.count) return 0;
    
    u8 c = s.data[0];
    for (u64 i = 0; i < match.count; i++) {
        if (match.data[i] == c) return 1;
    }
    
    return 0;
}

u8 string_ends_with_any_u8(String s, String match) {

    if (!s.count) return 0;
    
    u8 c = s.data[s.count - 1];
    for (u64 i = 0; i < match.count; i++) {
        if (match.data[i] == c) return 1;
    }
    
    return 0;
}

u8 string_starts_with(String s, String match) {
    if (s.count < match.count) return 0;
    for (u64 i = 0; i < match.count; i++) {
        if (s.data[i] != match.data[i]) return 0;
    }
    return 1;
}

u8 string_ends_with(String s, String match) {
    if (s.count < match.count) return 0;
    u64 pos = s.count - match.count;
    for (u64 i = 0; i < match.count; i++) {
        if (s.data[pos + i] != match.data[i]) return 0;
    }
    return 1;
}

// note: we return error by set the .data = NULL, which is not that obvious
String string_find_u8(String s, u8 c) {
    for (u64 i = 0; i < s.count; i++) {
        if (s.data[i] == c) return (String) {s.data + i, s.count - i};        
    }
    return (String) {0};
}

// note: we return error by set the .data = NULL, which is not that obvious
String string_find_u8_from_end(String s, u8 c) {
    for (u64 i = s.count; i > 0;) {
        i--;
        if (s.data[i] == c) return (String) {s.data + i, s.count - i};        
    }
    return (String) {0};
}

// todo: validate
// note: we return error by set the .data = NULL, which is not that obvious
String string_find_any_u8(String a, String match) {
    
    if (!a.count || !a.data) return (String) {0};

    u8 table[256] = {0};
    string_build_lookup_table(table, match);

    for (u64 i = 0; i < a.count; i++) {
        if (table[a.data[i]]) return (String) { a.data + i, a.count - i };        
    }
    
    return (String) {0};
}

// naive search for now
// todo: validate
// note: we return error by set the .data = NULL, which is not that obvious
String string_find(String a, String b) {
    if (!a.count || !b.count || !a.data || !b.data || (b.count > a.count)) return (String) {0};
    for (u64 i = 0; i < a.count - b.count + 1; i++) {
        for (u64 j = 0; j < b.count; j++) {
            if (a.data[i + j] != b.data[j]) goto next;
        }
        return (String) {a.data + i, a.count - i};
        next: continue;
    }
    return (String) {0};
}




/* ---- Trim & Eat Utils ---- */

u64 string_eat_until(String* s, u8 c) {
    
    u8* p     = s->data;
    u64 count = s->count;
    u64 out   = 0;
   
    for (u64 i = 0; i < count; i++) {
        if (p[i] != c)  out++;
        else            break;
    }
    
    s->data  += out;
    s->count -= out;

    return out;
}
   
u64 string_eat_matches(String* s, const u8 table[256]) {

    u8* p     = s->data;
    u64 count = s->count;
    u64 out   = 0;
   
    for (u64 i = 0; i < count; i++) {
        if (table[p[i]])  out++;
        else              break;
    }
    
    s->data  += out;
    s->count -= out;

    return out;
}

u64 string_eat_not_matches(String* s, const u8 table[256]) {

    u8* p     = s->data;
    u64 count = s->count;
    u64 out   = 0;
   
    for (u64 i = 0; i < count; i++) {
        if (!table[p[i]]) out++;
        else              break;
    }
    
    s->data  += out;
    s->count -= out;

    return out;
}




/* ---- Trim ---- */

// todo: validate
String string_trim_any_u8_from_start_using_table(String s, const u8 table[256]) {
    string_eat_matches(&s, table);
    return s;
}

// todo: validate
String string_trim_any_u8_from_end_using_table(String s, const u8 table[256]) {
    for (u64 i = s.count; i > 0;) {
        i--;
        if (!table[s.data[i]]) return (String) { s.data, i + 1 };
    }
    return (String) {0};
}

// todo: validate
String string_trim_any_u8_using_table(String s, const u8 table[256]) {
    string_eat_matches(&s, table);
    return string_trim_any_u8_from_end_using_table(s, table);
}

// todo: validate
String string_trim_any_u8_from_start(String s, String match) {
    u8 table[256] = {0};
    string_build_lookup_table(table, match);
    string_eat_matches(&s, table);
    return s;
}

// todo: validate
String string_trim_any_u8_from_end(String s, String match) {
    u8 table[256] = {0};
    string_build_lookup_table(table, match);
    return string_trim_any_u8_from_end_using_table(s, table);
}

// todo: validate
String string_trim_any_u8(String s, String match) {
    u8 table[256] = {0};
    string_build_lookup_table(table, match);
    return string_trim_any_u8_using_table(s, table);
}

String string_trim_spaces_from_start(String s) {
    return string_trim_any_u8_from_start_using_table(s, string_spaces_lookup_table);    
}

String string_trim_spaces_from_end(String s) {
    return string_trim_any_u8_from_end_using_table(s, string_spaces_lookup_table);    
}

String string_trim_spaces(String s) {
    return string_trim_any_u8_using_table(s, string_spaces_lookup_table);    
}




/* ---- Lazy Allocated Splitting ---- */

String string_eat_by_u8_separator(String* s, u8 c) {
    
    u8* data  = s->data;
    u64 count = string_eat_until(s, c);
   
    if (s->count) {
        s->data++;
        s->count--;
    }
    
    if (!count) return (String) {0};
    
    return (String) { data, count };
}

String string_eat_by_any_u8_matches_using_table(String* s, const u8 table[256]) {
   
    string_eat_not_matches(s, table);
    
    u8* data  = s->data;
    u64 count = string_eat_matches(s, table);
    
    string_eat_not_matches(s, table);
    
    return (String) { data, count };
}

String string_eat_by_any_u8_separators_using_table(String* s, const u8 table[256]) {

    string_eat_matches(s, table);
    
    u8* data  = s->data;
    u64 count = string_eat_not_matches(s, table);
    
    string_eat_matches(s, table);
    
    return (String) { data, count };
}

// note: this is slower than using_table version, but easier to use
String string_eat_by_any_u8_matches(String* s, String match) {
    u8 table[256] = {0};
    string_build_lookup_table(table, match);
    return string_eat_by_any_u8_matches_using_table(s, table);
}

// note: this is slower than using_table version, but easier to use
String string_eat_by_any_u8_separators(String* s, String separators) {
    u8 table[256] = {0};
    string_build_lookup_table(table, separators);
    return string_eat_by_any_u8_separators_using_table(s, table);
}

String string_eat_by_spaces(String* s) {
    return string_eat_by_any_u8_separators_using_table(s, string_spaces_lookup_table);
}

String string_eat_line(String* s) {
    String line = string_eat_by_u8_separator(s, '\n');
    if (!line.count) return line;
    if (line.data[line.count - 1] == '\r') line.count--;
    return line;
}

// todo: cleanup and validate
String string_eat_by_separator(String* s, String separator) {
    
    String found = string_find(*s, separator);
    if (!found.count) {
        String out = *s;
        *s = (String) {0};
        return out; 
    }
    
    String out = { s->data, (u64) found.data - (u64) s->data };
    *s = string_advance(*s, out.count + separator.count);

    return out;
}

// todo: should we make this the default?
// todo: cleanup and validate
String string_eat_by_separator_excluding_empty(String* s, String separator) {
    
    u8* data  = NULL;     
    u64 count = 0;
    
    while (!count) {
        
        String found = string_find(*s, separator);
        if (!found.count) {
            String out = *s;
            *s = (String) {0};
            return out; 
        }
        
        data  = s->data;
        count = (u64) found.data - (u64) data;
    
        *s = string_advance(*s, count + separator.count);
    }
    
    return (String) { data, count };
}







// todo: validate
u8 parse_u64(String s, u64* out) {

    if (!s.count) return 0;

    *out = 0;

    u64 result = 0;
    u64 digit_count = 0;

    for (u64 i = 0; i < s.count; i++) {
        
        u8 c = s.data[i];
        if (c == '_') {
            if (i != 0) continue;
            else        return 0;
        } else {
            if (c < '0' || c > '9') return 0;
        }
        
        result *= 10;
        result += c - '0';
        
        digit_count++;
        if (digit_count > 20) return 0; // todo: not handling all overflows
    }
    
    *out = result;
    
    return 1;
}




/* ==== Standard IO ==== */

// basic print
void print_string(String s) {
    fwrite(s.data, sizeof(u8), s.count, stdout);
}

// todo: not robust, need more testing, handle adjacent items (no space in between)
void print(String s, ...) {
    
    va_list args;
    va_start(args, s);
    
    for (u64 i = 0; i < s.count; i++) {

        u8 c = s.data[i];
        if (c == '@') {
            if (i + 1 < s.count && s.data[i + 1] == '@') { // short circuit 
                putchar('@');
                i++;
            } else {
                print_string(va_arg(args, String)); // not safe, but this is C varargs, what can you do 
            }
            continue;
        }

        putchar(c);
    }
    
    va_end(args);
}


void file_print_string(FILE* f, String s) {
    fwrite(s.data, sizeof(u8), s.count, f);
}

// todo: not robust, need more testing, handle adjacent items (no space in between)
void file_print(FILE* f, String s, ...) {
    
    va_list args;
    va_start(args, s);
    
    for (u64 i = 0; i < s.count; i++) {

        u8 c = s.data[i];
        if (c == '@') {
            if (i + 1 < s.count && s.data[i + 1] == '@') { // short circuit 
                fputc('@', f);
                i++;
            } else {
                file_print_string(f, va_arg(args, String)); // not safe, but this is C varargs, what can you do 
            }
            continue;
        }

        fputc(c, f);
    }
    
    va_end(args);
}

void file_print_quoted_string(FILE* f, String s) {

    fputc('"', f);
    
    for (u64 i = 0; i < s.count; i++) {
        
        u8 c = s.data[i];
        switch (c) {
            
            case '"':  fprintf(f, "\\\""); break;
            case '\\': fprintf(f, "\\\\"); break;
            case '\b': fprintf(f, "\\b");  break;
            case '\f': fprintf(f, "\\f");  break;
            case '\n': fprintf(f, "\\n");  break;
            case '\r': fprintf(f, "\\r");  break;
            case '\t': fprintf(f, "\\t");  break;
           
            default: 
            {
                if (c < 0x20) fprintf(f, "\\u%.4x", c);
                else          fputc(c, f);     
                break;
            }
        }
    }
    
    fputc('"', f);
}

// note: does not give a copy
String read_line() {
    
    String s = context.input_buffer;

    fgets((char*) s.data, s.count, stdin);
    s.count = strlen((const char*) s.data);
    
    if (s.count == 0) return (String) {0};
    if (s.data[s.count - 1] == '\n') s.count -= 1;
    
    return s;
}





/* ==== File IO ==== */

// todo: do we really need to switch allocator?
String load_file(char* path) {

    FILE* f = fopen(path, "rb");
    if (!f) return (String) {0}; // todo: this is not enough, what if we have a file of size 0? 

    fseek(f, 0, SEEK_END);
    u64 count = ftell(f);
    fseek(f, 0, SEEK_SET);

    u8* data = context.alloc(count);
    if (!data) {
        fclose(f);
        return (String) {0}; 
    }
    
    fread(data, 1, count, f);
    fclose(f);

    return (String) {data, count};
}

u8 save_file(String in, char* path) {

    FILE* f = fopen(path, "wb");
    if (!f) return 0; 

    fwrite(in.data, sizeof(u8), in.count, f);
    fflush(f);
    fclose(f);

    return 1;
}


